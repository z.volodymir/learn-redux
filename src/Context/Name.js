import React from "react";
import MyContext from "./Context";

export default function Name () {
	return (
		<MyContext.Consumer>
			{(value) => {
				return(
					<div>
						<h1>My name is: {value.name}</h1>
					</div>
				)
			}}
		</MyContext.Consumer>
	)
}
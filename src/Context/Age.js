import React from "react";
import MyContext from "./Context";

export default class Name extends React.Component{
	render() {
		return (
			<div>
				<h1>My age is : {this.context.age}</h1>
			</div>
		)
	}
}

Name.contextType = MyContext
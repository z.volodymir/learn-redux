import React from "react";
import MyContext from "./Context";
import Name from "./Name";
import Age from "./Age";

export default function App () {
	return (
		<MyContext.Provider value={{
			name: 'Vladimir',
			age: 30
		}}>
			<div>
				<Name/>
				<hr/>
				<Age/>
			</div>
		</MyContext.Provider>
	)
}
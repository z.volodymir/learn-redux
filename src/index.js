import {createStore} from "redux";

// редюсер
const reducer = (state = 23, action) => {
	switch (action.type) {
		case 'INC':
			return  state + 1;
		case 'DEC':
			return state - 1;
		case 'RES':
			return state = 0;
		default:
			return state
	}
}

//  создаем хранилище
const store = createStore(reducer);

// Вспомогательные функции
const inc = () => ({type: 'INC'});
const dec = () => ({type: 'DEC'});
const res = () => ({type: 'RES'});


//  Проверяем нажатия кнопок
function getElement(elem, type) {
	document.getElementById(elem).addEventListener('click', () => {
		store.dispatch(type);
	});
}

getElement('inc', inc());
getElement('dec', dec());
getElement('res', res());

//  функция обновляет counter
const update = () => {
	document.getElementById('counter').textContent = store.getState();
}
// при каждом изменении
store.subscribe(update);
